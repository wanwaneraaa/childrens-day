# 一、山师饭宝项目介绍
作品名称：山师饭宝                
团队名称：九一儿童节               
团队成员：侯宛辰、李鸿杉、蒋黎、熊淑贤、罗光懿  
## 1. 项目简介
#### 1.1 项目名称
山师饭宝

#### 1.2 项目背景
当代大学生基本一日三餐都在学校食堂里解决，现在的高校食堂菜品种类相对丰富，卫生条件良好，能让大学生吃得安心。虽然学校食堂的菜品很多，种类丰富，但是每当饭点来临，很多人还是会受到“今天中午吃什么”的困扰，有的同学会连续几天吃同一种菜品，甚至很多同学在校期间只吃过几种菜品，不敢尝试新的菜品。
由此，我们梳理为以下两个问题：
（1）菜品种类丰富，同学们吃过的菜品少；
（2）很多同学每天为吃什么饭发愁，思来想去，走走转转，很浪费时间。
同时，我们总结出了其中的原因：
（1）不敢尝试新品，怕踩坑；
（2）习惯了几种菜品以后，就没有了尝试新菜品的欲望。

#### 1.3 项目目的
根据以上总结的问题和原因，九一儿童节团队开发了“山师饭宝”APP，以山师二餐二楼为例：
（1）设置美食评分列表，展示各个店铺的菜品和评分信息，并提供评分功能；
（2）设置“今天吃什么”九宫格，提供随机美食抽签功能。
目的在于给同学们选择菜品提供参考，并且在同学们不知道吃什么的时候为他们提供选择，希望同学们能每天好好吃饭。
 
#### 1.4 项目优势
（1）用户数量相对稳定。
我们的项目目标用户为山师的同学甚至老师，店铺目前为二餐二楼的各窗口。山师学生数量较多，并且每年面临毕业和新生，因此用户数量可以维持在比较稳定的值。
（2）拥有“山师情怀”。
目标用户为山师的师生，大家对山师有着特殊的情怀，该产品的菜品均为山师食堂内的，这些熟悉又陌生的东西会勾起大家的“山师情怀”，吸引大家的关注。
（3）内容相对稳定。
高校食堂内的窗口变化相对较少，偶尔会有新窗口产生，旧窗口营业不利倒闭的现象，或者窗口新菜品的增加，但是大多数窗口比较稳定，因此维护相对容易。

#### 1.5 项目前景
（1）菜品方面。
目前我们的店铺和菜品为长清湖校区二餐二楼的各个窗口，但是未来我们可以不仅局限于这一小部分，可以将一餐和二餐一楼的窗口、甚至千佛山校区的窗口添加进来，扩大菜品的选择范围。
（2）用户方面。
现阶段我们的项目针对的是山师的师生，但是我们所总结出的菜品选择问题不仅山师的学生会遇到，全国高等学校共计2956所，高校生数量几千万人，数量庞大，我们可以将项目推广至其他各个高校，用户的覆盖面会更加广泛。用户基数庞大后，可以考虑与高校食堂经营者合作，为店铺提供广告的投放平台，因此我们的项目拥有广阔的前景和商业价值。


## 2. 项目构思

#### 2.1 大框构思
本项目主要实现三个功能：评分功能、抽签功能、用户功能。
（1）评分功能。
用户可以对自己所吃过的菜品进行评分，平台收集各用户对菜品打的分数，计算平均分，该评分为用户提供选择菜品的参考。
（2）抽签功能。
设置“今天吃什么”九宫格页面，可实现随机美食抽签功能，用户可以在不知道吃什么的时候用该功能进行随机的美食选择，靠天吃饭。
（3）用户功能。
每个用户拥有自己的独立账号，可以实现每天打卡、留言反馈、设置等功能。


#### 2.2 细节构思

###### 2.2.1 主页页面排版构思
我们对主页的排版进行了多种设计，最终按用户的习惯敲定了比较符合现在大众习惯的排版，在主页面展示店铺的实物图、名称和评分。

![输入图片说明](image/1.png)

该排版更简洁地展示了各店铺的名称、店铺菜品的实物图以及大家对其的评分，同时也符合大众的习惯。

###### 2.2.2 主题颜色构思
本产品的主题颜色为黄色，黄色属于暖色，更能激起人的食欲，我们对各种黄色的深浅明亮度进行挑选，选择了比较活泼明亮的#ffdc80黄色，该颜色在激起人的食欲的同时又比较时尚亮眼，不会显得土气，更符合年轻人的审美。
产品的各个页面都采用黄色为边框颜色，同时评分的星星颜色也采用黄色，主题色彩相呼应，显得页面干净整洁，不会太过花哨。

###### 2.2.3 图标构思
对于APP下方TabBar的图标，我们针对不同页面挑选了不同的图标。
①主页面的图标为叉子和勺子，符合餐饮的主题
②随机选择页面的图标是一个骰子，展示随机的特点
③用户页面是一个小人，符合用户的认知常识。
图标风格选择简笔画形式，更加简洁有趣又不失可爱。

![输入图片说明](image/2.png)

###### 2.2.4 图片选材构思

###### 2.2.5 LOGO构思
本产品的logo由本团队独立设计，是以一碗米饭为原型的小人头像，符合“饭宝”的名称。

![输入图片说明](image/3.png)

## 3. 功能实现

#### 3.1 开发环境
Windows10系统、HbuilderX、uniCloud、VScode

#### 3.2 团队分工
侯宛辰：项目策划；前端页面设计；前后端数据连接；评分功能；九宫格设计；搜索功能；真机运行

熊淑贤：后端数据库；前后端数据连接；评分功能；随机数生成；搜索功能；积分功能；签到功能

蒋黎：灵感提供；市场调研；素材收集；素材整理；真机运行

李鸿杉：项目策划；撰写项目介绍书；撰写运行教程；logo设计；产品测试

罗光懿：项目策划；PPT制作；项目展示；logo设计；产品测试

#### 3.3 素材收集
1、咨询店家关于二餐二楼布局编号等基本信息。
2、在二餐二楼拍摄店铺的招牌照片以及菜品照片。
3、将菜品一一用文字罗列出来，并对应相应的商家整理为一个Word文档。
4、将店铺招牌图片、店铺实物图片、店铺名称一一对应，命名整理为一个文件夹。

#### 3.4 页面设计

###### 3.4.1 页面一：“美食列表”
1、列表页由三个模块组成。
模块一：搜索（①）。
本模块的作用是方便用户查找列表中的店铺。
模块二：列表（②）。
本模块展示了店铺菜品图片、店铺名称、店铺评分。
模块三：详情页
详情页中展示了店铺招牌、店铺名称、店铺菜单，同时在详情页中有评分功能。

![输入图片说明](image/4.png)

2、点击列表中的店铺进入详情页，详情页主要用来展示招牌、菜单和评分。
①当前评分展示。
②店铺招牌图片。
③用户评分交互功能。
④店铺菜单。

![输入图片说明](image/5.png)

3、列表以及详情页中各店铺的名称、店铺招牌图片、店铺菜品图片、目录等数据均来自uniCloud的数据表，每一个店铺有其专属记录，其中，我们主要用到的信息有：
①id：每个店铺的专属id，用来做唯一标识，以保证数据的唯一性。	
②avatar：列表页每个店铺的菜品图片。
③content：详情页中每个店铺的菜单。
④title：店铺名称。
⑤food_rating：店铺的评分。
⑥total：评分的总次数。
⑦banner：详情页中店铺的招牌图片。

![输入图片说明](image/6.png)

4、评分功能的原理为：设置分数初始值为0，每当用户点击星星进行评分时，所评价的分数加上原先的分数与原先评价的次数的积，再除以总评价次数，得到的新数值就是该店铺的分数。公式如下：
新分数=（原分数*原评价次数+新评价分数）/（原评价次数+1）

###### 3.4.2 页面二：今天吃什么
1、本页面的主要功能为随机美食抽签功能。
2、本页面中使用随机九宫格，每次点击中央宫格后，都会为周围8个宫格分别随机更新8个店铺，然后开始在8个宫格上执行旋转抽签动画，旋转速度先加快后减慢，最后随机地停留在其中的一个宫格上，随后宫格反转，显示一个店铺名称，即为替同学选择的店铺。
3、本页面活泼美观，整体动画过程流畅有美感，提升用户使用体验。

![输入图片说明](image/7.png)

###### 3.4.3 页面三：“我的”
1、该界面主要以用户个人记录为主。
2、该模块中用户的信息同样来自uniCloud中的数据表，每个用户有一条单独的记录，记录中包含用户的信息。其中主要用到的信息有：
①id：每个用户有一个专属id，用来区分用户
②username：用户账号。
③password：用户账号的密码。
④nickname：用户昵称。
⑤avatar_file：用户头像图片。

![输入图片说明](image/8.png)

3、“我的”界面构成如下：
①每日签到：签到功能可以增加用户使用次数，签到可以获得积分。
②我的积分：可以查看已获得积分。
③问题与反馈：意见反馈为用户向平台进行的留言反馈，我们可以及时得到用户的反馈，对平台做出调整。
④设置：在设置中，用户可以清理缓存、注销账号和退出登录操作、也可以修改自己的个人头像、用户昵称、绑定手机号。
⑤关于：包括该平台的版本号，《用户服务协议》和《隐私政策》。

![输入图片说明](image/9.png)

# 二、山师饭宝运行教程
## 1. 运行步骤：
 
（1）在浏览器中搜索Gittee，并登录，然后在搜索框内搜索“wanwaneraaa”
![输入图片说明](image/image1.png)

（2）搜索出“wanwaneraaa/childrens-day”，并点击
![输入图片说明](image/image2.png)

（3）点击“sdnu-food-king v1.0.zip”
![输入图片说明](image/image3.png)

（4）点击下载，下载该zip文件
![输入图片说明](image/image4.png)
 
（5）下载后将zip文件解压缩
![输入图片说明](image/image5.png)

（6）打开HBuilder X，点击“登录”
![输入图片说明](image/image6.png)
 
（7）账号为“819954997@qq.com”，密码为“helloworld”（软件维护中，密码已更新）
![输入图片说明](image/image7.png)

（8）打开网址：https://ext.dcloud.net.cn/plugin?name=compile-node-sass
点击“使用HBuilderX导入插件”
![输入图片说明](image/image8.png)

（9）提示“登录原因”，点击“登录”
![输入图片说明](image/image9.png)
 
（10）账号密码与上面相同
![输入图片说明](image/image10.png)
 
（11）出现提示弹框，点击“继续”
![输入图片说明](image/image11.png)
 
（12）提示“此站点正在尝试打开HBuilderX”，点击“打开”
![输入图片说明](image/image12.png)
 
（13）提示需要升级HBuiderX，点击“升级”
![输入图片说明](image/image13.png)
 
（14）升级成功后，点击“立即重启”
![输入图片说明](image/image14.png)
 
（15）回到网页再次点击“使用HBuiderX导入插件”
![输入图片说明](image/image15.png)
 
（16）提示“是否安装【SCSS/SASS】插件”，点击“是”
![输入图片说明](image/image16.png)

（17）提示插件安装成功，点击“确定”
![输入图片说明](image/image17.png)

（18）点击“文件->导入->从本地目录导入”
![输入图片说明](image/image18.png)
 
（19）找到刚刚下载的位置，选择“sdnu-food-king”，点击“选择文件夹”
![输入图片说明](image/image19.png)
 
（20）点击“sdnu-food-king ->pages->list->list_nvue”
![输入图片说明](image/image20.png)
 
（21）在“uniCloud”出右键选择“关联云服务空间或项目”
![输入图片说明](image/image21.png)
 
（22）选择“wwr-starter-admin”，点击“关联”
![输入图片说明](image/image22.png)
 
（23）点击“预览”，如果提示“插件【内置浏览器】未安装，是否立即下载安装”，点击“是”
![输入图片说明](image/image23.png)
 
（24）提示“插件【内置浏览器】安装成功”后点击“确定”
![输入图片说明](image/image24.png)

（25）再次点击“预览”
![输入图片说明](image/image25.png)
 
（26）若提示防火墙阻止部分功能，点击“允许访问”
![输入图片说明](image/image26.png)
 
（27）等待运行
![输入图片说明](image/image27.png)
 
（28）运行成功
![输入图片说明](image/image28.png)


